const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/simulation', async (req, res) => {
  const { interestRate, loanAmount, days } = req.body;
  if (interestRate && loanAmount && days) {
    const total = parseFloat(loanAmount * (1 + interestRate / 100) ** days);
    const baseIof = parseFloat(loanAmount * (0.38 / 100));
    const iof = parseFloat(loanAmount * days * (0.0041 / 100));
    const totalIOF = baseIof + iof;
    return res.send({ value: total.toFixed(2), tax: totalIOF.toFixed(2) });
  }
  return res.status(400).send();
});
process.stdout.write(`PORT => ${process.env.PORT}`);
app.listen(process.env.PORT, () => {
  process.stdout.write('Server runing');
});
