FROM node:13.7.0-alpine3.10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .
EXPOSE 8084
ENV PORT=8084
CMD ["node", "index.js"]